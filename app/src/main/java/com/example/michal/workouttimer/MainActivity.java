package com.example.michal.workouttimer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getCanonicalName();

    TextView textView, setNumberTextView, textViewMilliseconds;

    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L;

    Handler handler;

    int Seconds, Minutes, MilliSeconds, startPause, setNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final List<String> quotations = getQuotations();

        Random rand = new Random();
        String random = quotations.get(rand.nextInt(quotations.size()));

        TextView quotationTextView =  findViewById(R.id.quotation);

        quotationTextView.setText("" + random);

        handler = new Handler();

        ImageView imageView = (ImageView) findViewById(R.id.aboutIcon);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aboutIconIntent = new Intent(MainActivity.this, AboutIconActivity.class);
                startActivity(aboutIconIntent);
            }
        });

        textView =  findViewById(R.id.textView);
        textViewMilliseconds =  findViewById(R.id.miliseconds);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (startPause == 0) {

                    StartTime = SystemClock.uptimeMillis();
                    handler.postDelayed(runnable, 0);
                    startPause = 1;
                    startToast();

                } else {

                    TimeBuff += MillisecondTime;
                    handler.removeCallbacks(runnable);
                    startPause = 0;
                    pauseToast();
                }
            }
        });

        textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                TimeBuff += MillisecondTime;
                handler.removeCallbacks(runnable);
                startPause = 0;

                MillisecondTime = 0L;
                StartTime = 0L;
                TimeBuff = 0L;
                UpdateTime = 0L;
                Seconds = 0;
                Minutes = 0;

                textView.setText("00:00");
                textViewMilliseconds.setText("000");

                Random rand = new Random();
                String random = quotations.get(rand.nextInt(quotations.size()));

                TextView quotationTextView = findViewById(R.id.quotation);
                quotationTextView.setText("" + random);

                resetToast();

                return true;
            }
        });

        Button resetButton = findViewById(R.id.minusButton);
        setNumberTextView = findViewById(R.id.setTexView);

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "On click pressed");
                setNumber = setNumber - 1;
                if (setNumber < 0) {
                    setNumber = 0;
                }
                //displaySetNumber(setNumber);
                setNumberTextView.setText(String.valueOf(setNumber));
                Log.d(TAG, "text view updated");
            }
        });

        resetButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.d(TAG, "On long click pressed");
                setNumberTextView.setText("0");
                setNumber = 0;
                Log.d(TAG, "text view updated");

                return true;
            }
        });
    }

    @NonNull
    private List<String> getQuotations() {
        final List<String> quotations = new ArrayList<>();
        final int quotationsAmount = 22;
        for (int i = 1; i <= quotationsAmount; ++i) {
            String packageName = MainActivity.class.getPackage()
                    .getName();
            final int nextQuotationId = getResources().getIdentifier("quotation" + i, "string", packageName);
            final String nextQuotation = getString(nextQuotationId);
            Log.i(this.getClass()
                    .getSimpleName(), "Adding quotation '" + nextQuotation + "'");
            quotations.add(nextQuotation);
        }
        return quotations;
    }

    public void decrementSet(View view) {

        setNumber = setNumber - 1;
        if (setNumber < 0) {
            setNumber = 0;
        }
        displaySetNumber(setNumber);
    }

    public void incrementSet(View view) {

        setNumber = setNumber + 1;
        displaySetNumber(setNumber);

    }

    private void displaySetNumber(int number) {
        TextView setNumberTextView = (TextView) findViewById(R.id.setTexView);
        setNumberTextView.setText("" + number);
    }

    public void startToast() {

        Toast.makeText(this, "Start", Toast.LENGTH_SHORT)
                .show();
    }

    public void pauseToast() {
        Toast.makeText(this, R.string.pause, Toast.LENGTH_SHORT)
                .show();
    }

    public void resetToast() {

        Toast.makeText(this, "Reset", Toast.LENGTH_LONG)
                .show();

    }

    public Runnable runnable = new Runnable() {

        public void run() {

            MillisecondTime = SystemClock.uptimeMillis() - StartTime;

            UpdateTime = TimeBuff + MillisecondTime;

            Seconds = (int) (UpdateTime / 1000);

            Minutes = Seconds / 60;

            Seconds = Seconds % 60;

            MilliSeconds = (int) (UpdateTime % 1000);

            textView.setText(String.format("%02d", Minutes) + ":"
                    + String.format("%02d", Seconds));

            textViewMilliseconds.setText(String.format("%03d", MilliSeconds));
            handler.postDelayed(this, 0);
        }

    };

    static String getRandomString() {
        int r = (int) (Math.random() * 5);
        String name = new String[]{"India", "USA", "UK", "Russia"}[r];
        return name;
    }
}
